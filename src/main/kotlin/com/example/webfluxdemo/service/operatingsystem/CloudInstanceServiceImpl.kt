package com.example.webfluxdemo.service.operatingsystem

import com.example.webfluxdemo.handler.AppUser
import com.example.webfluxdemo.model.dto.CloudInstanceDto
import com.example.webfluxdemo.model.request.CloudInstanceRequest
import com.example.webfluxdemo.repository.CloudInstanceRepository
import com.example.webfluxdemo.repository.OperatingSystemRepository
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

@Service
class CloudInstanceServiceImpl(
    val cloudInstanceRepository: CloudInstanceRepository,
    val operatingSystemRepository: OperatingSystemRepository,
    @Qualifier("UserClient") val userClient: WebClient
) : CloudInstanceService {
    override fun create(cloudInstanceRequest: CloudInstanceRequest): Mono<CloudInstanceDto> {
        return cloudInstanceRepository
            .save(cloudInstanceRequest.toEntity())
            .map { res -> res.toDto() }
    }

    override fun findAll(): Flux<CloudInstanceDto> {
        val cloudInstanceMono = cloudInstanceRepository
            .findAll()

        val osMono = cloudInstanceMono
            .flatMap {
                operatingSystemRepository.findById(it.operatingSystemId)
            }
        val userIdFlux: Flux<String> = cloudInstanceMono.map { it.userID }

        fun fetchUserById(id: String): Mono<AppUser> = userClient.get()
            .uri("/api/v1/users/{id}", id)
            .retrieve()
            .bodyToMono(AppUser::class.java)

        val zip_os = cloudInstanceMono.zipWith(osMono)
            .map {
                val cloud = it.t1
                val myos = it.t2

                val cloudResponse = cloud.toDto()
                cloudResponse.operatingSystem = myos.toDto()

                cloudResponse
            }
    return zip_os.zipWith(userIdFlux)
    .flatMap {
        val clouds = it.t1
        val userId = it.t2
        val user = fetchUserById(userId)

        Mono.just(clouds).zipWith(user)
            .map { resultTup ->
                val cloud = resultTup.t1
                val userResult = resultTup.t2
                cloud.user = userResult
                cloud
            }
            .log()
    }
}
}
